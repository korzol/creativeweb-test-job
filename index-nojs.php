<?php
    session_start();

    require_once "vladapps/testjob/src/loader.php";
    require_once "db_info.php";

    use Vladapps\Testjob\Validator\Validator;
    use Vladapps\Testjob\Model\Person;
    use Vladapps\Testjob\Model\PersonCollection;
    use Vladapps\Testjob\DB\Db;

    $success = '';

    // Checking if form was submitted
    if ( isset($_POST['fname']) )
    {
        $v = new Validator($_POST);
        /**
         * As of right now only two validation rules implemented:
         * email and required. However rules set can be easily
         * extended at any point later
         */
        $errors = $v->validate(
            [
                'email'     => 'email|required',
                'fname'     => 'required',
                'lname'     => 'required',
                'address'   => 'required',
                'postcode'  => 'required',
                'country'   => 'required',
                'phone'     => 'required',
            ]
        );

        // Checking if there are any errors
        if ( count($errors->getAll()) )
        {
            foreach ($errors->getAll() as $e)
            {
                $error[$e->getFieldName()] = $e->getErrorMessage();
            }
        }
        else
        {
            // No errors - create Person instance and save it to db
            $person = Person::create(
                $_POST['fname'],
                $_POST['lname'],
                $_POST['address'],
                $_POST['postcode'],
                $_POST['country'],
                $_POST['phone'],
                $_POST['email'],
            );

            $db = new Db($host, $db, $user, $pass);
            $res = $db->insert($person);

            // Save successful message into session
            $_SESSION['success'] = "Done. Provided data were saved in DB";

            // reload the pagge in order to prevent multi form submission
            header("Location: ".$_SERVER['PHP_SELF']);
            exit();

        }
    }

    // if there message is defined then output it and wipe out session - simple flash mechanism
    if ( isset($_SESSION['success']) )
    {
        $success = $_SESSION['success'];
        unset($_SESSION['success']);
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="css/screen.css">
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 style="color: #269e35; text-align: center"><?=$success?></h3>
            </div>
            <div class="col-12">
                <form id="personForm" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
                    <div class="form-group">
                        <label for="fname">First Name</label>
                        <input
                            type="text"
                            class="form-control"
                            name="fname"
                            value="<?=($_POST['fname'] ?? false)?>"
                            id="fname"
                            placeholder="Enter first name">
                        <label for="fname" class="error"><?=($error['fname'] ?? false)?></span>
                    </div>
                    <div class="form-group">
                        <label for="lname">Last Name</label>
                        <input
                            type="text"
                            class="form-control"
                            name="lname"
                            value="<?=($_POST['lname'] ?? false)?>"
                            id="lname"
                            placeholder="Enter surname">
                        <label for="lname" class="error"><?=($error['lname'] ?? false)?></span>
                    </div>

                    <div class="form-group">
                        <label for="address">Address</label>
                        <input
                            type="text"
                            class="form-control"
                            name="address"
                            value="<?=($_POST['address'] ?? false)?>"
                            id="address"
                            placeholder="Address">
                        <label for="address" class="error"><?=($error['address'] ?? false)?></span>
                    </div>

                    <div class="form-group">
                        <label for="postcode">Post Code</label>
                        <input
                            type="text"
                            class="form-control"
                            name="postcode"
                            value="<?=($_POST['postcode'] ?? false)?>"
                            id="postcode"
                            placeholder="Post Code">
                        <label for="postcode" class="error"><?=($error['postcode'] ?? false)?></span>
                    </div>

                    <div class="form-group">
                        <label for="country">Country</label>
                        <input
                            type="text"
                            class="form-control"
                            name="country"
                            value="<?=($_POST['country'] ?? false)?>"
                            id="country"
                            placeholder="Country">
                        <label for="country" class="error"><?=($error['country'] ?? false)?></span>
                    </div>

                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input
                            type="email"
                            class="form-control"
                            name="email"
                            value="<?=($_POST['email'] ?? false)?>"
                            id="email"
                            placeholder="Enter email">
                        <label for="email" class="error"><?=($error['email'] ?? false)?></span>
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input
                            type="phone"
                            class="form-control"
                            name="phone"
                            value="<?=($_POST['phone'] ?? false)?>"
                            id="phone"
                            placeholder="Enter phone">
                        <label for="phone" class="error"><?=($error['phone'] ?? false)?></span>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
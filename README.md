Requirements  

1. Create a Name and Address data capture form. First name, Surname, Address, Country, Postcode, Phone, Email  
2. On submission of data, validate the fields correctly, both client and server side  
3. Report any issues back in the form  
4. Store the data in a permanent data store  
5. Create an admin report that contains the contents of the data store as a nicely formatted list  
6. Submit project to a repository and supply us with access  
7. Please style everything so it presents well and is clear, bonus points if responsive  
8. Feel free to host a working demo somewhere and give us a link  
9. Please comment thoroughly throughout the code and please do not use any pre-built classes  

index.html          - is a fully-featured form page  
index-nojs.php      - fallback form page (disabled js)  
  
collected-data.html - Output collected data  
  
Frontend and backend are separated, except in index-nojs.php because of obvious reason.  
  
Live demo:  
[Form page](https://creativeweb.webinnolab.com/index.html)  
[No js form page](https://creativeweb.webinnolab.com/index-nojs.php)  
["Admin report" page](https://creativeweb.webinnolab.com/collected-data.html)  
  
Admin report page contains two tables one of which is responsive in "standard" bootstrap way. And the second one is responsive in some non standard way - where each tr become a "card" on small devices (< 600px)
<?php

    require_once "vladapps/testjob/src/loader.php";
    require_once "db_info.php";

    use Vladapps\Testjob\DB\Db;

    // Instantiate DB class and do select all entries
    $db = new Db($host, $db, $user, $pass);
    $res = $db->selectAll(2);

    // build entries array
    foreach ($res->getAll() as $person)
    {
        $array[] = [
            'id'        => $person->getPersonId(),
            'fname'     => $person->getFirstname(),
            'lname'     => $person->getSurname(),
            'address'   => $person->getAddress(),
            'postcode'  => $person->getPostcode(),
            'country'   => $person->getCountry(),
            'phone'     => $person->getPhone(),
            'email'     => $person->getEmail(),
        ];
    }

    // convert array tp json and output it
    echo json_encode($array);
<?php

namespace Vladapps\Testjob\Rules;

abstract class ValidationRules implements ValidationRulesInterface
{
    public static function validate(string $field): ValidationRules
    {
        return new static($field);
    }
}
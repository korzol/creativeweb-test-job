<?php
declare(strict_types=1);

namespace Vladapps\Testjob\Rules;

class EmailRule extends ValidationRules
{
    /**
     * $_POST field to be validated
     * @var string
     */
    private $field;

    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * Test value against predefined rule
     * @return bool
     */
    public function test(): bool
    {
        return (filter_var($this->field, FILTER_VALIDATE_EMAIL) ? true : false);
    }

    /**
     * Getter for errorMessage
     * @return string
     */
    public function errorMessage(): string
    {
        return "Please enter valid email address";
    }
}
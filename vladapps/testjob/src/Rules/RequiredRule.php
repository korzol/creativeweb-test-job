<?php
declare(strict_types=1);

namespace Vladapps\Testjob\Rules;

class RequiredRule extends ValidationRules
{
    /**
     * $_POST field to be validated
     * @var string
     */
    private $field;

    public function __construct(string $field)
    {
        $this->field = $field;
    }

    /**
     * Test value against predefined rule
     * @return bool
     */
    public function test(): bool
    {
        return ( !empty($this->field) ? true : false);
    }

    /**
     * Getter for errorMessage
     * @return string
     */
    public function errorMessage(): string
    {
        return "This field is required";
    }
}
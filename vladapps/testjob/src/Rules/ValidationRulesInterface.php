<?php

namespace Vladapps\Testjob\Rules;

interface ValidationRulesInterface
{
    public static function validate(string $field): ValidationRules;
    public function test(): bool;
}
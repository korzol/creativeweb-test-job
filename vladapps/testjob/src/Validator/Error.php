<?php

declare(strict_types = 1);

namespace Vladapps\Testjob\Validator;

final class Error
{
    /**
     * $_POST field name
     * @var string
     */
    private $fieldName;

    /**
     * Error message
     * @var string
     */
    private $errorMessage;

    public static function create(
        string $fieldName,
        string $errorMessage
    ): self {
        return new self (
            $fieldName,
            $errorMessage
        );
    }

    /**
     * Getter for $fieldMame (it is $_POST key)
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * Getter for $errorMessage
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function __construct(string $fieldName, string $errorMessage)
    {
        $this->fieldName    = $fieldName;
        $this->errorMessage = $errorMessage;
    }
}
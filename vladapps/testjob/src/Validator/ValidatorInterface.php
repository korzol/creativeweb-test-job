<?php

namespace Vladapps\Testjob\Validator;

use Vladapps\Testjob\Rules\EmailRule;
use Vladapps\Testjob\Rules\RequiredRule;

interface ValidatorInterface
{
    public function validate(array $rules);
}
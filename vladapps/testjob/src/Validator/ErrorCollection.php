<?php

declare(strict_types = 1);

namespace Vladapps\Testjob\Validator;

final class ErrorCollection
{
    /**
     * Array of Error instances
     * @var array
     */
    private $collection = [];

    public function __construct(?array $errors = [])
    {
        foreach($errors as $error)
        {
            $this->add($error);
        }
    }

    /**
     * Add Error instance into collection
     * @param Error $error
     */
    public function add(Error $error): void
    {
        $this->collection[$error->getFieldName()] = $error;
    }

    /**
     * Remove Error from the collection
     * @param  Error $error Error Instance
     * @return void
     */
    public function remove(Error $error): void
    {
        unset($this->collection[$error->getFieldName()]);
    }

    /**
     * Get defined Error instance from the collection
     * @param  string $fieldName
     * @return Error           Error Instance
     */
    public function get(string $fieldName): Error
    {
        return $this->collection[$fieldName];
    }

    /**
     * Get Error instances array
     * @return array Error instances array
     */
    public function getAll(): array
    {
        return $this->collection;
    }
}
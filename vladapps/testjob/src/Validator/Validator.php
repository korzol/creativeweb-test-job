<?php

declare(strict_types=1);

namespace Vladapps\Testjob\Validator;

use Vladapps\Testjob\Validator\Error;
use Vladapps\Testjob\Validator\ErrorCollection;

class Validator implements ValidatorInterface
{
    /**
     * $_POST
     * @var array
     */
    private $post;

    public function __construct(array $post)
    {
        $this->post = $post;
    }

    /**
     * Validate predefined $_POST fields agains predefined validation rules
     * @param  array  $vRules Array field => rules
     * @return ErrorCollection ErrorCollection instance
     */
    public function validate(array $vRules): ErrorCollection
    {
        $errors = [];
        foreach ($vRules as $key => $rules)
        {
            foreach (explode("|", $rules) as $rule)
            {
                $r = "Vladapps\Testjob\Rules\\".ucfirst($rule)."Rule::validate";

                $validate = $r($this->post[$key]);

                if ( ! $validate->test() )
                {
                    $errors[] = Error::create($key, $validate->errorMessage());
                }
            }

        }
        return new ErrorCollection($errors);
    }
}
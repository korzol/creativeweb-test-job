<?php

declare(strict_types = 1);

namespace Vladapps\Testjob\Model;

final class PersonCollection
{
    /**
     * Array of Person instances
     * @var array
     */
    private $collection = [];

    public function __construct(?array $persons = [])
    {
        foreach($persons as $person)
        {
            $this->add($person);
        }
    }

    /**
     * Add Person instance into collection
     * @param Person $person
     */
    public function add(Person $person): void
    {
        $this->collection[$person->getPersonId()] = $person;
    }

    /**
     * Remove Person from the collection
     * @param  Person $person Person Instance
     * @return void
     */
    public function remove(Person $person): void
    {
        unset($this->collection[$person->getPersonId()]);
    }

    /**
     * Get defined Person instance from the collection
     * @param  string $personId Person id
     * @return Person           Person Instance
     */
    public function get(string $personId): Person
    {
        return $this->collection[$personId];
    }

    /**
     * Get Person instances array
     * @return array Person instances array
     */
    public function getAll(): array
    {
        return $this->collection;
    }
}
<?php
declare(strict_types = 1);

namespace Vladapps\Testjob\Model;

final class Person
{
    /**
     * Person id
     * @var int
     */
    private $personId;

    /**
     * Person first name
     * @var string
     */
    private $firstname;

    /**
     * Person surname
     * @var string
     */
    private $surname;

    /**
     * Person address
     * @var string
     */
    private $address;

    /**
     * Person post code
     * @var string
     */
    private $postcode;

    /**
     * Person country
     * @var string
     */
    private $country;

    /**
     * Person phone
     * @var string
     */
    private $phone;

    /**
     * Person email
     * @var string
     */
    private $email;

    public static function create(
        string $firstname,
        string $surname,
        string $address,
        string $postcode,
        string $country,
        string $phone,
        string $email,
        ?int   $personId = null
    ): self {
        return new self (
            $firstname,
            $surname,
            $address,
            $postcode,
            $country,
            $phone,
            $email,
            $personId
        );
    }

    /**
     * Getter for persons first name
     * @return string First name
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * Getter for persons last name
     * @return string Surname (Last name)
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * Getter for persons address
     * @return string Address
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * Getter for persons post code
     * @return string Post code
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * Getter for persons country
     * @return string Country
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * Getter for persons phone
     * @return string Phone
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * Getter for persons email
     * @return string Email
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Getter for persons id
     * @return int Id
     */
    public function getPersonId(): int
    {
        return $this->personId;
    }

    public function __construct(
        string $firstname,
        string $surname,
        string $address,
        string $postcode,
        string $country,
        string $phone,
        string $email,
        ?int   $personId = null
    ) {
        $this->firstname    = $firstname;
        $this->surname      = $surname;
        $this->address      = $address;
        $this->postcode     = $postcode;
        $this->country      = $country;
        $this->phone        = $phone;
        $this->email        = $email;
        $this->personId     = $personId;
    }
}
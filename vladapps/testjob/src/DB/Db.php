<?php

declare(strict_types=1);

namespace Vladapps\Testjob\DB;

use Vladapps\Testjob\Model\Person;
use Vladapps\Testjob\Model\PersonCollection;
use PDO;

class Db
{
    /**
     * DB Host
     * @var string
     */
    private $host;

    /**
     * DB name
     * @var string
     */
    private $db;

    /**
     * DB username
     * @var string
     */
    private $user;

    /**
     * DB user password
     * @var string
     */
    private $pass;

    /**
     * DB charset
     * @var string
     */
    private $charset;

    /**
     * PDO instance
     * @var PDO
     */
    private $pdo;

    public function __construct(string $host, string $db, string $user, string $pass, ?string $charset='utf8mb4' )
    {
        $this->host     = $host;
        $this->db       = $db;
        $this->user     = $user;
        $this->pass     = $pass;
        $this->charset  = $charset;
        $this->pdo      = $this->connect();
    }

    /**
     * Connect to DB using provided info
     * @return PDO PDO Instance
     */
    public function connect(): PDO
    {
        $dsn = "mysql:host=".$this->host.";dbname=".$this->db.";charset=".$this->charset;
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
             return new PDO($dsn, $this->user, $this->pass, $options);
        } catch (PDOException $e) {
             throw new PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    /**
     * Insert new person into DB
     * @param  Person $person Instance of Person class
     * @return string         Insert id
     */
    public function insert(Person $person): string
    {
        //print_r($this->pdo); exit();
        $sql = "
                INSERT INTO
                            `people`
                        SET
                            `fname`     = ?,
                            `lname`     = ?,
                            `address`   = ?,
                            `postcode`  = ?,
                            `country`   = ?,
                            `phone`     = ?,
                            `email`     = ?
                ";
        $array = [
            $person->getFirstname(),
            $person->getSurname(),
            $person->getAddress(),
            $person->getPostcode(),
            $person->getCountry(),
            $person->getPhone(),
            $person->getEmail(),
        ];
        $this->pdo->prepare($sql)->execute($array);

        return $this->pdo->lastInsertId();
    }

    /**
     * Select defined person information
     * @param  int    $id Person id
     * @return Person Person instance
     */
    public function selectOne(int $id): Person
    {
        $sql    = "SELECT * FROM `people` WHERE `id` = ? LIMIT 1";
        $stmt   = $this->pdo->prepare($sql);
        $stmt->execute([$id]);
        $item   = $stmt->fetch(PDO::FETCH_OBJ);

        // it is possible to fetch data right into predefined class
        // but there is a problem namespaces. Left here for information only
        /*$item    = $stmt->fetchObject('Person', [
            'fname',
            'lname',
            'address',
            'postcode',
            'country',
            'phone',
            'email',
            'id',
        ]);
        */

        return Person::create(
            $item->fname,
            $item->lname,
            $item->address,
            $item->postcode,
            $item->country,
            $item->phone,
            $item->email,
            $item->id,
        );
    }

    /**
     * Select all entries
     * @return PersonCollection Collection of person instances
     */
    public function selectAll(): PersonCollection
    {
        $sql    = "SELECT * FROM `people`";
        $stmt   = $this->pdo->query($sql);
        $rows   = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($rows as $item)
        {
            $people[] = Person::create(
                            $item->fname,
                            $item->lname,
                            $item->address,
                            $item->postcode,
                            $item->country,
                            $item->phone,
                            $item->email,
                            $item->id,
                        );
        }

        return new PersonCollection($people);
    }
}
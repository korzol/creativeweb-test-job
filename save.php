<?php

    require_once "vladapps/testjob/src/loader.php";
    require_once "db_info.php";

    use Vladapps\Testjob\Validator\Validator;
    use Vladapps\Testjob\Model\Person;
    use Vladapps\Testjob\Model\PersonCollection;
    use Vladapps\Testjob\DB\Db;


    $v = new Validator($_POST);
    /**
     * As of right now only two validation rules implemented:
     * email and required. However rules set can be easily
     * extended at any point later
     */
    $errors = $v->validate(
        [
            'email'     => 'email|required',
            'fname'     => 'required',
            'lname'     => 'required'
            'address'   => 'required'
            'postcode'  => 'required'
            'country'   => 'required'
            'phone'     => 'required'
        ]
    );

    // Checking if there are any errors
    if ( count($errors->getAll()) )
    {
        $output = [
            'status' => 'error',
        ];

        $i = 0;
        foreach ($errors->getAll() as $e)
        {
            $output['fields'][$i]['field']    = $e->getFieldName();
            $output['fields'][$i]['message']  = $e->getErrorMessage();
            $i++;
        }

        echo json_encode($output);
    }
    else
    {
        // No errors - create Person instance and save it to db
        $person = Person::create(
            $_POST['fname'],
            $_POST['lname'],
            $_POST['address'],
            $_POST['postcode'],
            $_POST['country'],
            $_POST['phone'],
            $_POST['email'],
        );

        $db = new Db($host, $db, $user, $pass);
        $res = $db->insert($person);
        echo $res; // last insert id
    }



